#!/bin/sh


ID=$(ceph osd create)
mkdir /var/lib/ceph/osd/ceph-$ID

ceph-osd -i $ID --mkfs
ceph-osd -i $ID -d
