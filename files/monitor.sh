#!/bin/sh -x

echo "Start manager $MONNODE"
ip a s


if [ x$MONNODE == "x0" ]
then 
ceph-mon -i cluster$MONNODE --mkfs
ceph-mon -i cluster$MONNODE -d
else
ceph mon getmap -o /tmp/out$MONNODE
ceph-mon -i cluster$MONNODE --mkfs --monmap /tmp/out$MONNODE
ceph-mon -i cluster$MONNODE -d

fi
