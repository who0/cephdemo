#!/bin/sh

radosgw -i gnw
radosgw-admin user create --uid=admin --display-name="Admin" --system --admin

ACCESS=$(radosgw-admin user info --uid=admin |jq -r ".keys[0].access_key")
SECRET=$(radosgw-admin user info --uid=admin |jq -r ".keys[0].secret_key")

echo $ACCESS  | ceph dashboard set-rgw-api-access-key -i -
echo $SECRET  | ceph dashboard set-rgw-api-secret-key -i -

sleep 50
ceph mgr module disable dashboard
ceph mgr module enable dashboard

tail -f /dev/null
