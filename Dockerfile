FROM alpine
RUN apk add ceph  py3-pip ceph-mgr ceph-radosgw mailcap jq
RUN apk add py3-pip py3-openssl py3-scipy py3-yaml py3-jwt py3-jinja2 py3-bcrypt py3-werkzeug py3-dateutil py3-jsonpatch
RUN pip install prettytable pecan cherrypy routes
ADD etc /etc
### fix for diskprediction_hang
RUN rm -rf /usr/share/ceph/mgr/diskprediction_local/
