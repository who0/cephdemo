#!/bin/bash

echo -n "Default docker subnet : "
docker network inspect bridge|jq -r ".[0]|.IPAM.Config[0].Subnet" 

echo "Ctrl+C > Update etc/ceph/ceph.conf"
read


docker build -t ceph .

mkdir ceph
mkdir ceph/mon
mkdir ceph/osd

mkdir ceph/osd/ceph-0
mkdir ceph/osd/ceph-1
mkdir ceph/osd/ceph-2


node=(mon0 mon1 mon2 osd0 osd1 osd2 mds rgw)
#init

for n in ${node[@]}
do
echo "Start [$n]"
docker run -d --hostname $n --name $n -v $PWD/ceph:/var/lib/ceph:z ceph tail -f /dev/null
done


docker run -d --hostname mgr --name mgr -v $PWD/ceph:/var/lib/ceph:z -p 8080:8080 ceph tail -f /dev/null

# MON 
docker exec -it mon0 ceph-mon -i cluster0 --mkfs
docker exec -it mon0 ceph-mon -i cluster0

for a in 1 2
do
docker exec -it mon$a ceph mon getmap -o /tmp/out$a
docker exec -it mon$a ceph-mon -i cluster$a --mkfs --monmap /tmp/out$a
docker exec -it mon$a ceph-mon -i cluster$a
done

#MGR
docker exec -it mgr ceph-mgr -i cluster


#Create OSD
docker exec -it --user ceph mon0 ceph osd create #id -> 0
docker exec -it --user ceph mon0 ceph osd create #id -> 1
docker exec -it --user ceph mon0 ceph osd create #id -> 2

# Create MDS
docker exec -it mds ceph-mds -i cluster

for a in 0 1 2
do
docker exec -it osd$a ceph-osd -i $a --mkfs
docker exec -it osd$a ceph-osd -i $a
done


#Create Pool
docker exec -it mon0 ceph osd pool create saveraw 	16
docker exec -it mon0 ceph osd pool create savemeta 	16
#Create Ceph
#docker exec -it mon0 ceph fs new cephsave saveraw savemeta


#### RGW

######## SETUP RADOS GW ##############
docker exec rgw radosgw -i gnw
docker exec rgw radosgw-admin user create --uid=admin --display-name="Admin" --system --admin   &> /dev/null
ACCESS=$(docker exec  rgw radosgw-admin user info --uid=admin |jq -r ".keys[0].access_key")
SECRET=$(docker exec  rgw radosgw-admin user info --uid=admin |jq -r ".keys[0].secret_key")

###### CONNECT MGR TO RADOSGW ########
#DashBoard
docker exec -it mgr ceph config set mgr mgr/dashboard/ssl false
docker exec -it mgr ceph mgr module enable dashboard
echo $ACCESS | docker exec -i mgr ceph dashboard set-rgw-api-access-key -i -
echo $SECRET | docker exec -i mgr ceph dashboard set-rgw-api-secret-key -i -


### Generate password
PASS=$(dd status=none if=/dev/urandom of=/dev/stdout bs=1 count=15|base64)
echo $PASS | docker exec -i mgr tee /tmp/pass
docker exec -it mgr ceph dashboard ac-user-create admin -i /tmp/pass  administrator

#ceph osd pool application enable <pool-name> cephfs
#docker exec -it mon0 ceph osd pool application enable saveraw rbd
#ceph osd pool application enable <pool-name> rgw

###### STATUS 
docker exec -it --user ceph mon0 ceph -s

#### 
echo "################################"
IPMGR=$(docker inspect mgr |jq -r ".[0]|.NetworkSettings.Networks.bridge.IPAddress")
IPOBJ=$(docker inspect rgw |jq -r ".[0]|.NetworkSettings.Networks.bridge.IPAddress")
echo "http://$IPMGR:8080"
echo "User: admin"
echo "Pass: $PASS"

echo "--------------"
echo "Radosgw"
echo "http://$IPOBJ:7480"
